import { Component, ViewChild, ViewContainerRef, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { UiService } from './shared/ui/ui.service';
import { RadSideDrawerComponent } from 'nativescript-ui-sidedrawer/angular';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';

@Component({
  selector: 'app-root',
  templateUrl: "./app.component.html",
  moduleId: module.id
})
export class AppComponent {
  @ViewChild(RadSideDrawerComponent) drawerComponent: RadSideDrawerComponent;
  activeChallenge = '';
  private drawerSub: Subscription;
  private drawer: RadSideDrawer;
  private vcRef: ViewContainerRef;

  constructor(
    private uiService: UiService,
    private changeDetectionRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.drawerSub = this.uiService.drawerState.subscribe(() => {
      if (this.drawer) {
        this.drawer.toggleDrawerState();
      }
    });

    this.uiService.setRootVcRef(this.vcRef);
  }

  ngAfterViewInit() {
    this.drawer = this.drawerComponent.sideDrawer;
    this.changeDetectionRef.detectChanges();
  }

  onChallengeInput(challengeDescription: string){
      this.activeChallenge = challengeDescription;
      console.log('onChallengeInput: ', challengeDescription);
    }
    onLogout() {
      console.log("logout")
        this.uiService.toggleDrawer();
      }

      ngOnDestroy() {
        if (this.drawerSub) {
          this.drawerSub.unsubscribe();
        }
      }
}
