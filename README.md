## Software Dependencies
| SO | Tool |
| ------ | ------ |
| Mac OS | [xcode](https://developer.apple.com/xcode/) |
| Windows / Mac OS | [Android Studio](https://developer.android.com/studio/?gclid=EAIaIQobChMI7p2Utsb_6QIVhbLICh3HxAbKEAAYASAAEgJUg_D_BwE&gclsrc=aw.ds) |

## Installation
requires [Node.js](https://nodejs.org/) v12+ to run.
Use the package manager [nvm](https://github.com/nvm-sh/nvm) to install.

```bash
$ nvm install 12
$ nvm use 12.x.x
```
## Install the Angular CLI
Angular CLI v9 
```sh
$ npm install -g @angular/cli --save-dev
```
## Setting & nativescript-cli setup
```sh
$ npm install -g nativescript
```
| setting | link |
| ------ | ------ |
| quick-setup | [https://docs.nativescript.org/angular/start/quick-setup](https://docs.nativescript.org/angular/start/quick-setup) |
| Advanced Setup - Windows | [https://docs.nativescript.org/angular/start/ns-setup-win](https://docs.nativescript.org/angular/start/ns-setup-win) |
| Advanced Setup - macOS  | [https://docs.nativescript.org/angular/start/ns-setup-os-x](https://docs.nativescript.org/angular/start/ns-setup-os-x) |
## Usage
puede ejecutarse en dispositivos conectados a través de usb y simuladores de android y ios usando  el flag ```--bundle```
```bash
$ tns run ios
$ tns run android
```
Para modo debug usar el prefijo  ```debug```
```bash
$ tns debug ios --bundle
$ tns debug android --bundle
```
